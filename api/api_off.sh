#!/bin/bash
set -exou pipefail
date
echo "      bash processes"
ps -ef | grep bash 
echo "      python processes"
ps -ef | grep python
pkill -f gunicorn || echo "gunicorn failed to stop"