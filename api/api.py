from flask import Flask, request, jsonify, redirect, send_file
import logging
from flask_cors import CORS, cross_origin
from pathlib import Path
import sys, os, time, json

import database

from datetime import datetime
app = Flask(__name__)

@app.route("/test", methods=["POST", "GET"])
@cross_origin()
def test():
    print("test")
    return make_response({"working": 1, })

@app.route("/codes", methods=["POST", "GET"])
@cross_origin()
def codes():
    statement = """SELECT id, table_name, code, name, required 
                    FROM udc_code
                    order by table_name;"""
    return make_response(database.get_data(statement))

@app.route("/values", methods=["POST", "GET"])
@cross_origin()
def values():
    statement = """SELECT id, code, code_name, table_name, abv, name 
                    FROM udc_view;"""
    return make_response(database.get_data(statement))

@app.route("/master_values", methods=["POST", "GET"])
@cross_origin()
def master_values():
    statement = """SELECT id, code, description, table_name, abv, name 
                    FROM udc_master_view;"""
    return make_response(database.get_data(statement))

@app.route("/add_master_value", methods=["POST", "GET"])
@cross_origin()
def add_master_value():
    value_id = response.json['value_id']
    statement = f"""INSERT INTO udc_master (value_id)
                    VALUES({value_id});"""
    return make_response(database.add_data(statement))

@app.route("/remove_master_value", methods=["POST", "GET"])
@cross_origin()
def remove_master_value():
    value_id = response.json['value_id']
    statement = f"""DELETE FROM udc_master
                    WHERE value_id = {valuue_id};"""
    return make_response(database.add_data(statement))



def make_response(jdat):
    response = jsonify(jdat)
    response.headers.add("Access-Control-Allow-Headers", "Content-Type,Authorization")
    response.headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
    return response

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=443, debug=True)