#!/bin/bash
set -exou pipefail

pip3 install gunicorn || echo "gunicorn failed"
pip3 install flask  || echo "flask failed"
pip3 install flask_cors || echo "flask_cors failed"
pip3 install requests || echo "requests failed"

pip3 install slack || echo "slack failed"
pip3 install slackclient || echo "slack client install failed"