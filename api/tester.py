import requests
import time
from datetime import datetime
import boto3
import slack
import json, os


def send(message, number, cli):
    response = cli.publish(PhoneNumber=number, Message=message)
    if response['ResponseMetadata']["HTTPStatusCode"] == 200:
        return True
    return False


def sendStatusToSlack(status, slack_token):
    print("Sending message to slack!")
    client = slack.WebClient(token=slack_token)
    client.chat_postMessage(channel='support', text=status)


def loadkeys():
    f = open("key.key", "r")
    jdat = json.loads(f.readlines()[0])
    key_id = jdat["key_id"]
    access_key = jdat["access_key"]
    slack_token = jdat["slack_token"]
    return key_id, access_key, slack_token


def loadFile(s3):
    bucket = s3.Bucket('magyk-applications')
    jsonfile = bucket.Object(key='api-watcher/config/config.json')
    response = jsonfile.get()
    lines = response['Body'].read()
    decoded_json = lines.decode('ascii')
    full = "".join(decoded_json)
    load = json.loads(full)
    return load


def uploadFile(load, s3):
    out = open("out.json", "w")
    out.write(json.dumps(load))
    out.close()
    #print(json.dumps(load))
    s3.meta.client.upload_file("out.json", 'magyk-applications', "api-watcher/config/config.json")
    print("S3 Updated")

def check_website(server):
    status = pingServer(server["url"])
    name = server['name']   
    if str(status) == "200":
        print(f"{name} passed.")
    else:
        if server["state"] == 2 or server["state"] == "2":
            server["state"] = "2"
            print(f"{name} under maintaince")
        else:
            server["state"] = "-1"
            print(f"{name} did not responsed")
    return server

def check_api(server):
    status = postServer(server["url"])
    name = server['name']
    if str(status) == "200":
        print(f"server: '{name}' passed.")
        server["state"] = 1
    else:
        if server["state"] == 2 or server["state"] == "2":
            server["state"] = "2"
            print(f"server '{name}' under maintenance")
        else:
            server["state"] = "-1"
            print(f"server '{name}' did not respond")
    return server


def parseFile(load, cli, slack_token):
    count = int(load["counter"]["count"])
    count += 1
    heartbeat = False
    production = False

    if count > 900:
        count = 0
        heartbeat = True
    load["counter"]["count"] = str(count)
    for serv in load["servers"]:
        server = load["servers"][serv]
        if server['api'] == 0:
            load["servers"][serv] = check_website(server)
        if server['api'] == 1:
            load["servers"][serv]= check_api(server)
            
    for serve in load["servers"]:
        server = load['servers'][serve]
        if int(server['state']) < 1:
            print(f"server:{server['name']} state {server['state']}")
            heartbeat = True

        
    msg = formatmessage(load)

    if heartbeat:
        sendStatusToSlack(msg, slack_token)
        if production:
            for contact in load["contacts"]:
                print("PBC2 message to: " + load["contacts"][contact]["number"])
                send(msg, load["contacts"][contact]["number"], cli)



def pingServer(url):
    req = requests.get(url)
    return req.status_code

def postServer(url):
    try:
        req = requests.post(url)
        return req.status_code

    except Exception as e:
        print(f'Exception: {e}')
        print(f'data: {url}')
        return -6

def formatmessage(load):
    msg = gettime() + "\n"
    for server in load["servers"]:
        state = load["servers"][server]["state"]
        if state == "1":
            state = "working"
        elif state == "-1":
            state = "not working"
        elif state == "2":
            state = "ignored"
        msg += f"{load['servers'][server]['name']} {state} \n"
    return msg


def gettime():
    time = datetime.utcnow()
    return f"UTC-4: {time.date()} {time.hour - 4}:{time.minute}:{time.second}"


def main():
    key_id, access_key, slack_token = loadkeys()
    cli = boto3.client(
        "sns",
        aws_access_key_id=key_id,
        aws_secret_access_key=access_key,
        region_name="us-east-1"
    )

    s3 = boto3.resource(
        service_name='s3',
        region_name='us-east-1',
        aws_access_key_id=key_id,
        aws_secret_access_key=access_key
    )
    load = loadFile(s3)
    parseFile(load, cli, slack_token)
    uploadFile(load, s3)


def lambda_handler(event, context):
    main()


if __name__ == "__main__":

    while 1==1:
        main()
        print('Sleep 15: {}'.format(gettime()))
        time.sleep(15)
