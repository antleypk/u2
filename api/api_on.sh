#!/bin/bash
set -exou pipefail
date
echo "bash processes"
ps -ef | grep bash 
echo "python processes"
ps -ef | grep python
#pkill -f gunicorn || echo "gunicorn ready"
gunicorn --bind 0.0.0.0:7000 api:app --daemon
