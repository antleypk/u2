import psycopg2, json, os
from datetime import datetime
from os import walk


def get_cursor():
    cur = None
    try:
        conn = psycopg2.connect(
            "dbname=u2 host=localhost user=end_user password=insaan"
        )
        cur = conn.cursor()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    return cur, conn


def get_data(statement):
    cur, conn = get_cursor()
    cur.execute(statement)
    rv_statement = cur.fetchall()
    print("rv length: {}\n\nstatment: {}".format(len(rv_statement), statement))
    conn.close()
    return rv_statement

def add_data(statement):
    cur, conn = get_cursor()
    cur.execute(statement)
    print("\n\nstatment: {}".format(statement))
    conn.commit()
    conn.close()
    return True