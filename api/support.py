from datetime import datetime
import boto3, json, os
import tester 


def loadkeys():
    f = open("key.key", "r")
    jdat = json.loads(f.readlines()[0])
    key_id = jdat["key_id"]
    access_key = jdat["access_key"]
    return key_id, access_key

def uploadFile(load, s3):
    out = open("out.json", "w")
    out.write(json.dumps(load))
    out.close()
    print(json.dumps(load))
    s3.meta.client.upload_file("out.json", 'magyk-applications', "api-watcher/config/config.json")
    os.remove("out.json")
    print("uploaded")

def printer(jdat):    
    for key in jdat:
            print(f'    {key}: {jdat[key]} \n')


def extractFile(s3):
    bucket = s3.Bucket('magyk-applications')
    jsonfile = bucket.Object(key='api-watcher/config/config.json')
    response = jsonfile.get()
    lines = response['Body'].read()
    decoded_json = lines.decode('ascii')
    full = "".join(decoded_json)
    load = json.loads(full)
    return load

def gettime():
    time = datetime.utcnow()
    return "UTC-4: " + str(time.date()) + " " + str(time.hour - 4) + ":" + str(time.minute) + ":" + str(time.minute)

def ignore(server_id, jdat):
    for key in jdat['servers']:
        if str(key) == str(server_id):
            jdat['servers'][key]['state'] = 2
    return jdat

def activate(server_id, jdat):
    for key in jdat['servers']:
        if str(key) == str(server_id):
            jdat['servers'][key]['state'] = 1
    return jdat

def list_servers(jdat):
    b = jdat['servers']
    print("     Servers ")
    for a in b:
        c = b[a]
        print(f"   id: {a}, name: {c['name']}, state: {c['state']} api: {c['api']}\n url: {c['url']} \n")

def save_local(data):
    with open('tmp_config.json', 'w') as f:
        f.write(json.dumps(data))

def open_local():
    with open('tmp_config.json', 'r') as f:
        return json.loads(f.readlines()[0])
        
def add_server():
    try:
        server_id = input("server id: ")
        int(server_id)
    except TypeError:
        print("error, id must be int")
        return False

    name = input("name: ")
    url = input("url: ")
    state = input("state: ")
    api = int(input("api: 1/0: "))
    return {"id": server_id, "name": name, "state": state, "url": url, "api": api}

def remove_server(data):
    print('\n warning this only removes server from data in local memory')
    try:
        server_id = input("enter server id: ")
        del data['servers'][server_id]
        return data

    except TypeError:
        print('id must be int')
        return False
    except Exception as e:
        print(f'exception: {e} \n data: { data["servers"]}')

def cli(s3, data):
    run = True
    while run:
        print('0: exit')
        print('1: list servers')
        print('2: ignore servers')
        print('3: activate servers')
        print('4: save changes to remote')
        print('5: refresh data from bucket')
        print('6: sent notifaction to slack')
        print('7: make local copy of json')
        print('8: open local json')
        print('9: add server')
        print('10: remove server')
        response = input('Select a number: ')
        if response == '0':
            run = False
        if response == '1':
            print(1)
            list_servers(data)
        if response == '2':
            server_id = input('enter a server id: ')
            data = ignore(server_id, data)
        if response == '3':
            server_id = input('enter a server id: ')
            data = activate(server_id, data)
        if response == '4':
            uploadFile(data, s3)
        if response == '5':
            data = extractFile(s3)
        if response == '6':
            tester.main()
        if response == '7':
            save_local(data)
        if response == '8':
            data = open_local()
        if response == '9':
            d = add_server()
            if d:
                data['servers'][d['id']] = {"name":d["name"], "state": d["state"], "url": d["url"], "api": d['api']}  
        if response == '10':
            rv_data = remove_server(data)
            if rv_data:
                data = rv_data
        
        print(' ')

def main():
    key_id, access_key = loadkeys()
    s3 = boto3.resource(
        service_name='s3',
        region_name='us-east-1',
        aws_access_key_id=key_id,
        aws_secret_access_key=access_key
    )

    data = extractFile(s3)
    cli(s3, data)

if __name__ == "__main__":
    main()
